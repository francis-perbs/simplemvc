﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment
{
    class AssignmentSortClient
    {
        private IStringSorter stringSorter;

        public AssignmentSortClient(IStringSorter sorter)
        {
            stringSorter = sorter;
        }

        public void SetSortClient(IStringSorter newSorter)
        {
            stringSorter = newSorter;
        }

        public List<char> SortStringMethod(List<char> unsorted)
        {
            return stringSorter.SortString(unsorted);
        }
    }
}
