﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment
{
    public delegate void Change_SortMethod_Event<ISortView>(ISortView sender, ChangeSortMethodEventArgs e);

    public class ChangeSortMethodEventArgs : EventArgs
    {
        public int sortIndex;
        public ChangeSortMethodEventArgs(int index)
        {
            sortIndex = index;
        }
    }

    public delegate void Change_UnsortedString_Event<ISortView>(ISortView sender, ChangeUnsortedStringEventArgs e);

    public class ChangeUnsortedStringEventArgs : EventArgs
    {
        public string unsortedString;
        public ChangeUnsortedStringEventArgs(string uString)
        {
            unsortedString = uString;
        }
    }
    
    public interface ISortView
    {
        event Change_SortMethod_Event<ISortView> changeSortMethodEvent;
        event Change_UnsortedString_Event<ISortView> changeUnsortedStringEvent;
        void setController (ISortController controller);
    }

}
