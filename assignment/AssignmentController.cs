﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment
{
    public interface ISortController
    {
        void startSort();
        bool validateString();
        Object[] loadOptions();
    }

    public class AssignmentController : ISortController
    {
        ISortView sortView;
        ISortModel sortModel;

        public AssignmentController(ISortView sView, ISortModel sModel)
        {
            sortView = sView;
            sortModel = sModel;

            sortModel.attachObserver((ISortModelObserver) sortView);
            sortView.setController(this);
            sortView.changeSortMethodEvent += new Change_SortMethod_Event<ISortView>(this.change_sort_method);
            sortView.changeUnsortedStringEvent += new Change_UnsortedString_Event<ISortView>(this.change_unsorted_string);
        }

        public void change_sort_method(ISortView view, ChangeSortMethodEventArgs e)
        {
            sortModel.setSortMethod(e.sortIndex);
        }

        public void change_unsorted_string(ISortView view, ChangeUnsortedStringEventArgs e)
        {
            sortModel.setUnsortedString(e.unsortedString);
        }

        public void startSort()
        {
            sortModel.sortString();
        }

        public bool validateString()
        {
            return sortModel.validateUnsortedString();
        }

        public Object[] loadOptions()
        {
            return sortModel.loadSortOptions();
        }
    }
}
