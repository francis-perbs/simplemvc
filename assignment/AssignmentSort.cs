﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment
{
    public interface IStringSorter
    {
        List<char> SortString(List<char> unsorted);
    }
    
    public class BubbleSort : IStringSorter
    {
        List<char> list = new List<char>();
        public List<char> SortString(List<char> unsorted)
        {
            bool swapped = true;
            int prevLevel = 0;
            char temp;
            while(swapped)
            {
                swapped = false;
                prevLevel++;
                for(int x=0; x<unsorted.Count-prevLevel; x++)
                {
                    if(unsorted[x] > unsorted[x+1])
                    {
                        temp = unsorted[x];
                        unsorted[x] = unsorted[x+1];
                        unsorted[x+1] = temp;
                        swapped = true;
                    }
                }
            }

            return unsorted;
        }
    }

    public class MergeSort : IStringSorter
    {
        List<char> list = new List<char>();
        public List<char> SortString(List<char> unsorted)
        {
            List<char> result = new List<char>();
            List<char> left = new List<char>();
            List<char> right = new List<char>();

            if (unsorted.Count <= 1) return unsorted;

            int center = unsorted.Count / 2;
            for (int x=0; x<center; x++) left.Add(unsorted[x]);
            for (int x=center; x<unsorted.Count; x++) right.Add(unsorted[x]);

            left = SortString(left);
            right = SortString(right);

            if (left[left.Count - 1] <= right[0]) return Append(left, right);
            result = Merge(left, right);

            return result;
        }

        private List<char> Append(List<char> left, List<char> right)
        {
            List<char> tempResult = new List<char>(left);
            foreach (char item in right) tempResult.Add(item);
            return tempResult;
        }

        private List<char> Merge(List<char> left, List<char> right)
        {
            List<char> temp = new List<char>();
            while ((left.Count > 0) && (right.Count > 0))
            {
                if (left[0] < right[0])
                {
                    temp.Add(left[0]);
                    left.RemoveAt(0);
                }
                else
                {
                    temp.Add(right[0]);
                    right.RemoveAt(0);
                }
            }
            
            while (left.Count > 0)
            {
                temp.Add(left[0]);
                left.RemoveAt(0);
            }

            while (right.Count > 0)
            {
                temp.Add(right[0]);
                right.RemoveAt(0);
            }

            return temp;
        }
    }
}
