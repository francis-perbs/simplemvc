﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment
{
    public delegate void Sort_String_Event<ISortModel>(ISortModel sender, SortStringEventArgs e);

    public class SortStringEventArgs : EventArgs
    {
        public string sortedString;
        public SortStringEventArgs(string newString)
        {
            sortedString = newString;
        }
    }

    public interface ISortModelObserver
    {
        void displaySortedString(ISortModel model, SortStringEventArgs e);
    }
    
    public interface ISortModel
    {
        void attachObserver(ISortModelObserver modelObserver);
        void sortString();
        void setSortMethod(int sortIndex);
        void setUnsortedString(string unsortedString);
        bool validateUnsortedString();
        Object[] loadSortOptions();
    }

    public class AssignmentModel : ISortModel
    {
        public event Sort_String_Event<AssignmentModel> sortStringEvent;
        string unsortedString;
        string sortedString;
        int sortIndex;
        Object[] sortOptions = new Object[] {"Bubble Sort", "Merge Sort"};

        public AssignmentModel()
        {
            unsortedString = "";
            sortedString = "";
            sortIndex = -99998;
        }

        public void sortString()
        {
            if (unsortedString == "")
            {
                sortStringEvent.Invoke(this, new SortStringEventArgs(unsortedString));
                throw new Exception("Please input a string to sort.", null);
            }

            var sortClient = new AssignmentSortClient(new BubbleSort());
            
            switch(sortIndex) {
                case 0: 
                    sortClient.SetSortClient(new BubbleSort());
                break;

                case 1:
                    sortClient.SetSortClient(new MergeSort());
                break;

                default:
                    sortStringEvent.Invoke(this, new SortStringEventArgs(""));
                    throw new Exception("Sorting Method Not Defined.", null);
            }

            List<char> stringList = new List<char>();
            stringList = ConvertStringToList(unsortedString);

            stringList = sortClient.SortStringMethod(stringList);
            sortedString = ConvertListToString(stringList);
            sortStringEvent.Invoke(this, new SortStringEventArgs(sortedString));
        }

        public void setSortMethod(int index)
        {
            sortIndex = index;
        }

        public void setUnsortedString(string unsorted)
        {
            unsortedString = unsorted;
        }

        public void attachObserver(ISortModelObserver viewModel)
        {
            sortStringEvent += new Sort_String_Event<AssignmentModel>(viewModel.displaySortedString);
        }

        private List<char> ConvertStringToList(string unsorted)
        {
            List<char> list = new List<char>();
            for(int x=0; x<unsorted.Length; x++)
            {
                list.Add(unsorted[x]);
            }
            
            return list;
        }

        private string ConvertListToString(List<char> list)
        {
            return string.Join("", list);
        }

        public bool validateUnsortedString()
        {
            for(int x=0; x<unsortedString.Length; x++)
            {
                if((unsortedString[x] >= 'a') && (unsortedString[x] <= 'z')) continue; 
                else throw new Exception("Invalid Input String. Only a-z (small case) letters are allowed.", null);
            }

            return true;
        }

        public Object[] loadSortOptions()
        {
            return sortOptions;
        }
    }
}
