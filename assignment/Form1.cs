﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace assignment
{
    public partial class Form1 : Form, ISortView, ISortModelObserver
    {
        ISortController controller;
        public event Change_SortMethod_Event<ISortView> changeSortMethodEvent;
        public event Change_UnsortedString_Event<ISortView> changeUnsortedStringEvent;

        public void setController(ISortController cont)
        {
            controller = cont;
        }

        public Form1()
        {
            InitializeComponent();
        }

        public void displaySortedString(ISortModel model, SortStringEventArgs e)
        {
            label1.Text = "" + e.sortedString;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (controller.validateString()) controller.startSort();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                changeSortMethodEvent.Invoke(this, new ChangeSortMethodEventArgs(comboBox1.SelectedIndex));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void comboBox1_LostFocus(object sender, EventArgs e)
        {
            try
            {
                changeSortMethodEvent.Invoke(this, new ChangeSortMethodEventArgs(comboBox1.SelectedIndex));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                changeUnsortedStringEvent.Invoke(this, new ChangeUnsortedStringEventArgs(textBox1.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.Items.AddRange(controller.loadOptions());
        }
    }
}
